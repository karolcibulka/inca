<?php



use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(env('REDIRECT_HTTPS')){
    URL::forceScheme('https');
}

Route::get('/', 'Content\WelcomeController@index')->name('/');
Route::post('/sendEmail', 'Content\WelcomeController@sendEmail')->name('sendEmail');
Route::get('/setLocale/{lang}', 'Content\WelcomeController@setLocale')->name('setLocale');

Route::get('/service/{slug}','Content\ProjectController@showService')->name('service');
Route::get('/project/{slug}','Content\ProjectController@showProject')->name('project');

Route::resource('dashboard','Admin\DashboardController');
Route::resource('projects','Admin\ProjectController');
Route::resource('services','Admin\ServiceController');
Route::resource('reviews','Admin\ReviewController');
Route::resource('blog','Admin\BlogController');
Route::resource('messages','Admin\MessagesController');
Route::resource('team','Admin\TeamController');
Route::resource('gallery','Admin\GalleryController');
Route::resource('property','Admin\PropertyController');
Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
