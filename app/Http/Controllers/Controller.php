<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Project;
use App\Property;
use App\Review;
use App\Service;
use App\Team;
use Closure;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $currentLang;
    public $sendEmail;
    protected $sendEmailAt = '';
    protected $email = 'inca.sarl@yahoo.com';
    public $available_languages = ['fr', 'en'];
    protected $sizes;

    public function __construct()
    {

        if (isset($_GET['deleteCache'])) {
            $this->deleteFromCache('property');
            $this->deleteFromCache('reviews');
            $this->deleteFromCache('projects');
            $this->deleteFromCache('teams');
            $this->deleteFromCache('blogs');
            $this->deleteFromCache('services');
        }

        if(isset($_GET['sitemaps'])){
            $this->generateSiteMaps();
        }

        $this->sizes = array(
            'thumb' => 300,
            'medium' => 720,
            'large' => 1280,
        );

        $this->middleware(function ($request, Closure $next) {
            if (Session::has('sended_email_at')) {
                $time              = Session::get('sended_email_at');
                $this->sendEmailAt = strtotime($time);
                $minutes           = '20';
                $property          = $this->getContent('property');
                if (isset($property['minutes']) && !empty($property['minutes'])) {
                    $minutes = $property['minutes'];
                }
                $validate_time  = strtotime(date('Y-m-d H:i:s'));
                $validated_time = strtotime('+ ' . $minutes . ' minutes', $this->sendEmailAt);

                View::share('countdownTime', date('i', ($validated_time - $validate_time)));
                if ($validated_time > $validate_time) {
                    $this->sendEmail = true;
                } else {
                    $this->sendEmail = false;
                }
            } else {
                $this->sendEmail = false;
            }

            View::share('sendEmail', $this->sendEmail);

            return $next($request);
        });

        $this->currentLang = App::getLocale();
        View::share('currentLang', $this->currentLang);
    }

    public function deleteFromCache($pluralType)
    {
        if (Cache::has($pluralType)) {
            Cache::forget($pluralType);
        }
    }

    public function getContent($type = null)
    {

        $content = array(
            'reviews' => array(),
            'projects' => array(),
            'property' => array(),
            'teams' => array(),
            'blogs' => array(),
            'services' => array(),
        );

        if ($type) {
            switch ($type) {
                case 'reviews':
                    $reviews = array();
                    if (!$reviews = Cache::get('reviews')) {
                        $reviews = Review::where('active', '=', '1')->where('deleted', '=', '0')->get();
                        if (!empty($reviews)) {
                            foreach ($reviews as $review_key => $review) {
                                $reviews[$review_key]['text'] = json_decode($review['texts'], TRUE);
                            }
                        }
                        Cache::put('reviews', $reviews, 3600 * 24 * 7);
                    }
                    if (!is_array($reviews)) {
                        $reviews = $reviews->toArray();
                    }
                    return $reviews;
                    break;
                case 'teams':
                    $teams = array();
                    if (!$teams = Cache::get('teams')) {
                        $teams = Team::where('active', '=', '1')->get();
                        Cache::put('teams', $teams, 3600 * 24 * 7);
                    }
                    if (!is_array($teams)) {
                        $teams = $teams->toArray();
                    }
                    return $teams;
                    break;
                case 'services':
                    $services = array();
                    if (!$services = Cache::get('services')) {
                        $responseServices = Service::select('gi.name as image_name', 'services.names','services.short_texts', 'services.id','services.slug', 'services.texts')
                            ->where('services.active', '=', '1')
                            ->where('services.deleted', '=', '0')
                            ->leftJoin('galleryimages as gi', 'services.gallery_id', '=', 'gi.gallery_id')
                            ->get();


                        //dd($responseServices);

                        if (!empty($responseServices)) {
                            foreach ($responseServices as $key => $responseService) {
                                $image_name = '';
                                if (isset($responseService['names'], $responseService['short_texts']) && !empty($responseService['names']) && !empty($responseService['short_texts'])) {
                                    $services[$responseService['id']]['name']     = json_decode($responseService['names'], TRUE);
                                    $services[$responseService['id']]['short_text'] = json_decode($responseService['short_texts'], TRUE);
                                    $services[$responseService['id']]['slug']     = $responseService['slug'];
                                    $services[$responseService['id']]['text']     = json_decode($responseService['texts'],TRUE);
                                    $image_name                                   = explode('.', $responseService['image_name']);
                                    if (isset($image_name[0], $image_name[1]) && !empty($image_name)) {
                                        foreach ($this->sizes as $fit => $size) {
                                            $services[$responseService['id']]['images'][$key][$fit] = asset('uploads/' . $image_name['0'] . '-' . $fit . '.' . $image_name['1']);
                                        }
                                    } else {
                                        unset($services[$responseService['id']]);
                                    }
                                }
                            }
                        }

                        if (!empty($services)) {
                            foreach ($services as $key => $service) {
                                if (isset($service['images']) && !empty($service['images'])) {
                                    $services[$key]['images'] = array_values($services[$key]['images']);
                                }
                                if(!isset($service['slug']) || empty($service['slug'])){
                                    unset($services[$key]);
                                }
                            }
                        }

                        $services = array_values($services);

                        Cache::put('services', $services, 3600 * 24 * 7);
                    }

                    if (!is_array($services)) {
                        $services = $services->toArray();
                    }
                    return $services;
                    break;
                case 'projects':
                    $projects = array();
                    if (!$projects = Cache::get('projects')) {
                        $responseProjects = Project::select('gi.name as image_name', 'projects.names', 'projects.categories', 'projects.id','projects.slug', 'projects.texts')
                            ->where('projects.active', '=', '1')
                            ->where('projects.deleted', '=', '0')
                            ->leftJoin('galleryimages as gi', 'projects.gallery_id', '=', 'gi.gallery_id')
                            ->get();

                        if (!empty($responseProjects)) {
                            foreach ($responseProjects as $key => $responseProject) {
                                $image_name = '';
                                if (isset($responseProject['names'], $responseProject['categories']) && !empty($responseProject['names']) && !empty($responseProject['categories'])) {
                                    $projects[$responseProject['id']]['name']     = json_decode($responseProject['names'], TRUE);
                                    $projects[$responseProject['id']]['category'] = json_decode($responseProject['categories'], TRUE);
                                    $projects[$responseProject['id']]['slug']     = $responseProject['slug'];
                                    $projects[$responseProject['id']]['text']     = json_decode($responseProject['texts'],TRUE);
                                    $image_name                                   = explode('.', $responseProject['image_name']);
                                    if (isset($image_name[0], $image_name[1]) && !empty($image_name)) {
                                        foreach ($this->sizes as $fit => $size) {
                                            $projects[$responseProject['id']]['images'][$key][$fit] = asset('uploads/' . $image_name['0'] . '-' . $fit . '.' . $image_name['1']);
                                        }
                                    } else {
                                        unset($projects[$responseProject['id']]);
                                    }
                                }
                            }
                        }

                        //dd($r)

                        if (!empty($projects)) {
                            foreach ($projects as $key => $project) {
                                if (isset($project['images']) && !empty($project['images'])) {
                                    $projects[$key]['images'] = array_values($projects[$key]['images']);
                                }
                                if(!isset($project['slug']) || empty($project['slug'])){
                                    unset($projects[$key]);
                                }
                            }
                        }

                        //dd($projects);

                        Cache::put('projects', $projects, 3600 * 24 * 7);
                    }
                    if (!is_array($projects)) {
                        $projects = $projects->toArray();
                    }
                    return $projects;
                    break;
                case 'property':
                    $property = array();
                    if (!$property = Cache::get('property')) {
                        $property = Property::first();
                        if (isset($property['texts']) && !empty($property['texts'])) {
                            $property['text'] = json_decode($property['texts'], TRUE);
                        }
                        Cache::put('property', $property, 3600 * 24 * 7);
                    }
                    if (!is_array($property)) {
                        $property = $property->toArray();
                    }
                    return $property;
                case 'blogs':
                    $blogs = array();
                    if (!$blogs = Cache::get('blogs')) {
                        $blogs = Blog::where('active', '=', '1')->where('deleted', '=', '0')->get();
                        Cache::put('blogs', $blogs, 3600 * 24 * 7);
                    }
                    if (!is_array($blogs)) {
                        $blogs = $blogs->toArray();
                    }
                    return $blogs;
                    break;
            }
        }

        $content['reviews']  = $this->getContent('reviews');
        $content['teams']    = $this->getContent('teams');
        $content['projects'] = $this->getContent('projects');
        $content['services'] = $this->getContent('services');
        $content['property'] = $this->getContent('property');
        $content['blogs']    = $this->getContent('blogs');

        return $content;

    }

    private function generateSiteMaps()
    {
        $services = $this->getContent('services');
        $projects = $this->getContent('projects');

        if(!file_exists(public_path('/sitemap.xml'))){
            $brm = '<?xml version="1.0" encoding="UTF-8"?>';
            $brm.='<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            $brm.='<sitemap>';
            $brm.='<loc>'.url('/services.xml').'</loc>';
            $brm.='</sitemap>';
            $brm.='<sitemap>';
            $brm.='<loc>'.url('/projects.xml').'</loc>';
            $brm.='</sitemap>';
            $brm.='<sitemap>';
            $brm.='<loc>'.url('/basic.xml').'</loc>';
            $brm.='</sitemap>';
            $brm.='</sitemapindex>';
            $myfile = fopen(public_path('/sitemap.xml'), !file_exists(public_path('/sitemap.xml')) ? 'a' : 'w') or die('Unable to open file!');
            fwrite($myfile, $brm);
            fclose($myfile);
        }
        if(!file_exists(public_path('/sitemaps'))){
            mkdir(public_path('/sitemaps'), 0777);
        }
        if(!file_exists(public_path('/services.xml'))){
            $services = fopen(public_path('/services.xml'), "a") or die('Unable to open file!');
            fclose($services);
        }
        if(!file_exists(public_path('/services.xml'))){
            $projects = fopen(public_path('/services.xml'), "a") or die('Unable to open file!');
            fclose($projects);
        }
        if(!file_exists(public_path('/basic.xml'))){
            $basic = fopen(public_path('/basic.xml'), "a") or die('Unable to open file!');
            fclose($basic);
        }

        $s_links = '<?xml version="1.0" encoding="UTF-8"?>';
        $s_links .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if(!empty($services)){
            foreach($this->available_languages as $language){
                foreach($services as $service){
                    $s_links .= '<url>';
                    $s_links .='<loc>'.url($language.'/service/'.$service['slug']).'</loc>';
                    $s_links .='<lastmod>'.date('c').'</lastmod>';
                    $s_links .='<priority>1</priority>';
                    $s_links .='</url>';
                }
            }
        }
        $s_links .= '</urlset> ';


        $service_file = fopen(public_path('/services.xml'), !file_exists(public_path('/services.xml')) ? 'a' : 'w') or die('Unable to open file!');
        fwrite($service_file, $s_links);
        fclose($service_file);

        $p_links = '<?xml version="1.0" encoding="UTF-8"?>';
        $p_links .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if(!empty($projects)){
            foreach($this->available_languages as $language) {
                foreach ($projects as $project) {
                    $p_links .= '<url>';
                    $p_links .= '<loc>' . url($language . '/project/' . $project['slug']) . '</loc>';
                    $p_links .= '<lastmod>' . date('c') . '</lastmod>';
                    $p_links .= '<priority>1</priority>';
                    $p_links .= '</url>';
                }
            }
        }
        $p_links .= '</urlset> ';


        $projects_file = fopen(public_path('/projects.xml'), !file_exists(public_path('/projects.xml')) ? 'a' : 'w') or die('Unable to open file!');
        fwrite($projects_file, $p_links);
        fclose($projects_file);

        $b_links = '<?xml version="1.0" encoding="UTF-8"?>';
        $b_links .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        if(!empty($services)){
            foreach($this->available_languages as $language){
                $b_links .= '<url>';
                $b_links .='<loc>'.url($language).'</loc>';
                $b_links .='<lastmod>'.date('c').'</lastmod>';
                $b_links .='<priority>1</priority>';
                $b_links .='</url>';
            }
        }
        $b_links .= '</urlset> ';


        $basic_file = fopen(public_path('/basic.xml'), !file_exists(public_path('/basic.xml')) ? 'a' : 'w') or die('Unable to open file!');
        fwrite($basic_file, $b_links);
        fclose($basic_file);
    }
}
