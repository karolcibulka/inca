<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use App\Property;
use Illuminate\Http\Request;

class PropertyController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        $property = Property::first();
        $property['text'] = isset($property['texts']) && !empty($property['texts']) ? json_decode($property['texts'],TRUE) : array();
        return view($this->path.'property.show',['property'=>$property]);
    }

    public function update($lang,$id,Request $request){
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        unset($data['files']);
        if(isset($data['text'])){
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        Property::where('id','=',$id)->update($data);
        $this->deleteFromCache('property');
        return redirect(route('property.index'));
    }
}
