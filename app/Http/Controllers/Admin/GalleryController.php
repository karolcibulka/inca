<?php


namespace App\Http\Controllers\Admin;


use App\Gallery;
use App\Galleryimage;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class GalleryController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }


    public function index(){
        $galleries = Gallery::where('active','=','1')->where('deleted','=','0')->orderBy('id','desc')->get();
        return view($this->path.'gallery.show',['data'=>$galleries]);
    }

    public function create(){
        return view($this->path.'gallery.create');
    }

    public function edit($lang,$id){
        $gallery = Gallery::find($id);
        $images = Galleryimage::where('gallery_id','=',$id)->get();
        $imagesRes = array();
        if(!empty($images)){
            foreach($images as $key => $image){
                $exploded = explode('.',$image['name']);
                $imagesRes[$key]['name'] = asset('uploads/'.$exploded[0].'-thumb.'.$exploded[1]);
                $imagesRes[$key]['id'] = $image['id'];
            }
        }

        return view($this->path.'gallery.edit',['gallery'=>$gallery,'images'=>$imagesRes]);
    }

    public function update($lang,$id,Request $request){

        $data = $request->all();

        $gallery = array(
            'internal_name' => $data['internal_name'],
        );

        $res = Gallery::where('id','=',$id)->update($gallery);

        $images = array();
        if(isset($data['image']) && !empty($data['image'])){
            $files = $request->allFiles();
            if(isset($files['image']) && !empty($files['image'])){
                foreach($files['image'] as $image){
                    $time = uniqid().'_'.time();
                    $images[] = $time.'.jpg';
                    foreach($this->sizes as $fit => $size ){
                        $img = Image::make($image)->fit($size)->encode('jpg');
                        $name = $time .'-'.$fit.'.jpg';
                        Storage::disk('public')->put($name, $img);
                    }
                }
            }
        }

        $multipleImagesInsert = array();
        if(!empty($images)){
            foreach($images as $image){
                $imageInsert = array(
                    'gallery_id' => $id,
                    'name' => $image,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $multipleImagesInsert[] = $imageInsert;
            }
        }

        if(!empty($multipleImagesInsert)){
            Galleryimage::insert($multipleImagesInsert);
        }

        return redirect(route('gallery.index'));
    }

    public function destroy($lang,Request $request){
        $data = $request->all();

        $img = Galleryimage::find($data['id']);

        $exploded = explode('.',$img['name']);
        foreach($this->sizes as $type => $size){
            Storage::delete('public/image/'.$exploded[0].'-'.$type.'.'.$exploded[1]);
        }

        Galleryimage::where('id','=',$data['id'])->delete();

        return response(array('status' => '1'));
    }

    public function store(Request $request){
        $data = $request->all();
        $gallery = array(
            'internal_name' => $data['internal_name'],
            'active' => '1',
            'deleted'=>'0',
            'created_at' => date('Y-m-d H:i:s')
        );

        $gallery_id = Gallery::insertGetId($gallery);

        $images = array();
        if(isset($data['image']) && !empty($data['image'])){
            $files = $request->allFiles();
            if(isset($files['image']) && !empty($files['image'])){
                foreach($files['image'] as $image){
                    $time = uniqid().'_'.time();
                    $images[] = $time.'.jpg';
                    foreach($this->sizes as $fit => $size ){
                        $img = Image::make($image)->fit($size)->encode('jpg');
                        $name = $time .'-'.$fit.'.jpg';
                        Storage::put($name, $img);
                        Storage::move($name, 'public/image/' . $name);
                    }
                }
            }
        }

        $multipleImagesInsert = array();
        if(!empty($images)){
            foreach($images as $image){
                $imageInsert = array(
                    'gallery_id' => $gallery_id,
                    'name' => $image,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $multipleImagesInsert[] = $imageInsert;
            }
        }

        if(!empty($multipleImagesInsert)){
            Galleryimage::insert($multipleImagesInsert);
        }

        return redirect(route('gallery.index'));
    }

}
