<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\AdminController;

class DashboardController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }


    public function index(){
       return view($this->path.'dashboard.show');
    }
}
