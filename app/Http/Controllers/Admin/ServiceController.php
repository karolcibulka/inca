<?php


namespace App\Http\Controllers\Admin;


use App\Gallery;
use App\Http\Controllers\AdminController;
use App\Project;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServiceController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $projects = Service::where('deleted', '=', '0')->get();
        return view($this->path . 'services.show', ['data' => $projects]);
    }

    public function create()
    {
        $galleries = Gallery::where('active', '=', '1')->get();
        return view($this->path . 'services.create', ['galleries' => $galleries]);
    }

    public function edit($lang, $id)
    {
        $project             = Service::find($id);
        $galleries           = Gallery::where('active', '=', '1')->get();
        $project['name']     = json_decode($project['names'], TRUE);
        $project['short_text'] = json_decode($project['short_texts'], TRUE);
        $project['text']     = json_decode($project['texts'], TRUE);
        return view($this->path . 'services.edit', ['project' => $project, 'galleries' => $galleries]);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        unset($data['_token']);
        unset($data['files']);
        if (isset($data['name']) && !empty($data['name'])) {
            $data['names'] = json_encode($data['name']);
            unset($data['name']);
        }
        if (isset($data['short_text']) && !empty($data['short_text'])) {
            $data['short_texts'] = json_encode($data['short_text']);
            unset($data['short_text']);
        }
        if (isset($data['text']) && !empty($data['text'])) {
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        if (isset($data['slug']) && !empty($data['slug'])) {
            $data['slug'] = Str::slug($data['slug']);
        } else {
            $data['slug'] = Str::slug($data['internal_name']);
        }


        $data['created_at'] = date('Y-m-d H:i:s');

        Service::insert($data);

        $this->deleteFromCache('services');
        return redirect(route('services.index'));

    }

    public function update($lang, $id, Request $request)
    {
        $data = $request->input();

        //dd($data);
        unset($data['_token']);
        unset($data['files']);
        unset($data['_method']);
        if (isset($data['name']) && !empty($data['name'])) {
            $data['names'] = json_encode($data['name']);
            unset($data['name']);
        }
        if (isset($data['text']) && !empty($data['text'])) {
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        if (isset($data['short_text']) && !empty($data['short_text'])) {
            $data['short_texts'] = json_encode($data['short_text']);
            unset($data['short_text']);
        }
        if (isset($data['slug']) && !empty($data['slug'])) {
            $data['slug'] = Str::slug($data['slug']);
        } else {
            if(isset($data['internal_name']) && !empty($data['internal_name'])){
                $data['slug'] = Str::slug($data['internal_name']);
            }
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        Service::where('id', '=', $id)->update($data);

        $this->deleteFromCache('services');
        return redirect(route('services.index'));
    }
}
