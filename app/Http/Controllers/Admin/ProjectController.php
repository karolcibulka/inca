<?php


namespace App\Http\Controllers\Admin;


use App\Gallery;
use App\Project;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProjectController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        $projects = Project::where('deleted','=','0')->get();
        return view($this->path.'projects.show',['data'=>$projects]);
    }

    public function create(){
        $galleries = Gallery::where('active','=','1')->where('deleted','=','0')->orderBy('id','desc')->get();
        return view($this->path.'projects.create',['galleries'=>$galleries]);
    }

    public function edit($lang,$id){
        $project = Project::find($id);
        $galleries = Gallery::where('active','=','1')->where('deleted','=','0')->orderBy('id','desc')->get();

        $project['name'] = json_decode($project['names'],TRUE);
        $project['category'] = json_decode($project['categories'],TRUE);
        $project['text'] = json_decode($project['texts'],TRUE);

        return view($this->path.'projects.edit',['project'=>$project,'galleries'=>$galleries]);
    }

    public function store(Request $request){
        $data = $request->all();

        unset($data['_token']);
        unset($data['files']);
        if(isset($data['name']) && !empty($data['name'])){
            $data['names'] = json_encode($data['name']);
            unset($data['name']);
        }
        if(isset($data['category']) && !empty($data['category'])){
            $data['categories'] = json_encode($data['category']);
            unset($data['category']);
        }
        if(isset($data['text']) && !empty($data['text'])){
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        if(isset($data['slug']) && !empty($data['slug'])){
            $data['slug'] = Str::slug($data['slug']);
        }
        else{
            $data['slug'] = Str::slug($data['internal_name']);
        }


        $data['created_at'] = date('Y-m-d H:i:s');

        Project::insert($data);

        $this->deleteFromCache('projects');
        return redirect(route('projects.index'));

    }

    public function update($lang,$id,Request $request){
        $data = $request->input();

        //dd($data);
        unset($data['_token']);
        unset($data['files']);
        unset($data['_method']);
        if(isset($data['name']) && !empty($data['name'])){
            $data['names'] = json_encode($data['name']);
            unset($data['name']);
        }
        if(isset($data['category']) && !empty($data['category'])){
            $data['categories'] = json_encode($data['category']);
            unset($data['category']);
        }
        if(isset($data['text']) && !empty($data['text'])){
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        if(isset($data['slug']) && !empty($data['slug'])){
            $data['slug'] = Str::slug($data['slug']);
        }
        else{
            if(isset($data['internal_name']) && !empty($data['internal_name'])){
                $data['slug'] = Str::slug($data['internal_name']);
            }
        }

        $data['updated_at'] = date('Y-m-d H:i:s');

        Project::where('id','=',$id)->update($data);

        $this->deleteFromCache('projects');
        return redirect(route('projects.index'));
    }
}
