<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;

class MessagesController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        return view($this->path.'messages.show');
    }
}
