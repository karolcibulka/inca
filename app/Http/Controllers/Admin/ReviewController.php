<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends AdminController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = Review::all();
        return view($this->path . 'reviews.show', ['data' => $data]);
    }

    public function create(){
        return view($this->path.'reviews.create');
    }

    public function store(Request $request){
        $data = $request->all();
        unset($data['_token']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['texts'] = json_encode($data['text']);
        unset($data['text']);
        $data['active'] = '1';
        $data['deleted'] = '0';

        Review::insert($data);
        $this->deleteFromCache('reviews');
        return redirect(route('reviews.index'));
    }

    public function edit($lang,$id){
        $review = Review::find($id);
        $review['text'] = json_decode($review['texts'],TRUE);
        return view($this->path.'reviews.edit',['review'=>$review]);
    }

    public function update($lang,$id,Request $request){
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        if(isset($data['text'])){
            $data['texts'] = json_encode($data['text']);
            unset($data['text']);
        }
        Review::where('id','=',$id)->update($data);
        $this->deleteFromCache('reviews');
        return redirect(route('reviews.index'));
    }
}
