<?php


namespace App\Http\Controllers\Content;


use App\Http\Controllers\Controller;

class ProjectController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function showProject($lang,$slug){

        $content = $this->getContent();
        $content['row'] = $this->findMyRow($this->getContent('projects'),$slug);
        $content['lang'] = $lang;

        return view('_project.show',$content);
    }

    public function showService($lang,$slug){
        $content = $this->getContent();
        $content['row'] = $this->findMyRow($this->getContent('services'),$slug);
        $content['lang'] = $lang;

        return view('_project.show',$content);
    }

    private function findMyRow($content,$slug,$field = 'slug'){
       $my_item = array();

       if(!empty($content)){
           foreach($content as $content_item){
               if($content_item[$field] == $slug){
                   $my_item = $content_item;
                   break;
               }
           }
       }

       //dd($my_item);
       return $my_item;
    }
}
