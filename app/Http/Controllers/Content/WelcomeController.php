<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class WelcomeController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        return view('welcome',$this->getContent());
    }

    public function setLocale($lang){
        if(in_array($lang,$this->available_languages)){
            App::setLocale($lang);
        }
        return redirect()->route('/');

    }

    public function sendEmail(Request $request){

        $data = $request->all();

        $property = $this->getContent('property');

        Mail::send('_email.base', array('data'=>$data) , function ($m) use ($data,$property) {
            $m->from('no-reply@incasarl.fr', 'Inca Sarl System');
            $m->to(isset($property['email']) && !empty($property['email']) ? $property['email'] : $this->email)->subject('New message from website - '.$data['subject']);
        });

        unset($data['_token']);
        $data['active'] = '1';
        $data['deleted'] = '0';
        $data['created_at'] = date('Y-m-d H:i:s');
        Message::insert($data);

        Session::put('sended_email_at',date('Y-m-d H:i:s'));
        return redirect(url('/'.$this->currentLang.'#contact'));
    }


}
