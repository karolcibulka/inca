<?php


namespace App\Http\Controllers;

class AdminController extends Controller
{
    protected $path;

    public function __construct()
    {
        parent::__construct();
        $this->path = '_admin._content.';
    }

}
