@extends('_partials.master')

@section('content')
    <!-- Header Slider -->
    @include('_content.header')
    <!-- Services -->
    @include('_content.services')
    <!-- About -->
    @include('_content.about')
    <!-- Projects -->
    @include('_content.projects')
    <!-- Testimonials -->
    @include('_content.testimonials')
    <!-- Our Team -->
    @include('_content.team')
    <!-- Blog -->
    @include('_content.blog')
    <!-- Contact -->
    @include('_content.contact')
    <!-- Clients -->
    @include('_content.client')
@endsection
