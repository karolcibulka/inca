@extends('_partials.master')

@section('content')
    <div class="togo-side-content">
        <!-- Lines -->
        <div class="content-lines-wrapper">
            <div class="content-lines-inner">
                <div class="content-lines"></div>
            </div>
        </div>
        <!-- Post -->
        <section class="post mt-80 pb-100" data-scroll-index="5">
            <div class="container">
                <div class="row">
                    <!-- Content -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="item">
                                    @if(isset($row['images']) && !empty($row['images']))
                                        <div class="post-img">
                                            <div data-fancybox="gallery" href="{{$row['images'][0]['large']}}" class="img pointer" style="background-image:url({{$row['images'][0]['large']}});height:400px;background-position:center;background-size:cover;background-repeat: no-repeat;">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="cont">
                                        <h5>{{$row['name'][$lang]}}</h5>
                                        {!! $row['text'][$lang] !!}
                                        <div class="row mt-30 mb-30">
                                            @if(isset($row['images']) && !empty($row['images']))
                                                @foreach($row['images'] as $key => $image)
                                                   @if($key !== 0)
                                                        <div class="col-md-6">
                                                            <div class="post-img">
                                                                <div class="img pointer" data-fancybox="gallery" href="{{$image['large']}}" style="background-image:url({{$image['medium']}});height:300px;background-position:center;background-size:cover;background-repeat: no-repeat;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                   @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
