@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Reviews'}}</h5>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <form action="{{route('reviews.update',[$review['id']])}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>interný názov</label>
                                    <input type="text" name="internal_name" class="form-control" value="{{$review['internal_name']}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>meno</label>
                                    <input type="text" name="name" class="form-control" value="{{$review['name']}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>práca</label>
                                    <input type="text" name="work" class="form-control" value="{{$review['work']}}" required>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text fr</label>
                                    <textarea name="text[fr]" cols="30" rows="10" class="form-control" required>{{$review['text']['fr']}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text en</label>
                                    <textarea name="text[en]" cols="30" rows="10" class="form-control" required>{{$review['text']['en']}}</textarea>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button class="btn btn-primary w-100">Uložiť</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
