@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Reviews'}}</h5>
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('reviews.create')}}" class="btn btn-primary w-100">Vytvoriť</a>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>interný názov</th>
                                <th>meno</th>
                                <th>povolanie</th>
                                <th>aktivne</th>
                                <th>akcia</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($data) && !empty($data))
                                    @foreach($data as $d)
                                        <tr>
                                            <td>{{$d['id']}}</td>
                                            <td>{{$d['internal_name']}}</td>
                                            <td>{{$d['name']}}</td>
                                            <td>{{$d['work']}}</td>
                                            <td>
                                                <form action="{{route('reviews.update',[$d['id']])}}" method="post">
                                                    <div class="row">
                                                        <div class="col-md-12 ml-auto text-center">
                                                            @method('PUT')
                                                            @csrf
                                                            <input type="hidden" name="active" value="{{$d['active'] == '1' ? '0' : '1'}}">
                                                            <button class="btn btn-sm btn-{{$d['active'] == '1' ? 'success' : 'warning'}}">{{$d['active'] == '1' ? 'Aktívne' : 'Neaktívne'}}</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12 ml-auto text-center">
                                                        <a href="{{route('reviews.edit',[$d['id']])}}" class="btn btn-sm btn-info"> Upraviť</a>
                                                        <form action="{{route('reviews.update',[$d['id']])}}" method="post" style="display:inline-block">
                                                            <div class="row">
                                                                <div class="col-md-12 ml-auto text-center">
                                                                    @method('PUT')
                                                                    @csrf
                                                                    <input type="hidden" name="deleted" value="0">
                                                                    <button class="btn btn-sm btn-danger">Zmazať</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
