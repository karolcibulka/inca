@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Galérie'}}</h5>
                        </div>
                        <div class="col-md-6">
                            <a href="{{route('gallery.create')}}" class="btn btn-primary w-100">Vytvoriť</a>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>interný názov</th>
                                <th>akcia</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($data) && !empty($data))
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{$d['id']}}</td>
                                        <td>{{$d['internal_name']}}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-12 ml-auto text-center">
                                                    <a href="{{route('gallery.edit',[$d['id']])}}" class="btn btn-sm btn-info"> Upraviť</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
