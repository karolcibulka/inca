@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Galéria'}}</h5>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <form action="{{route('gallery.update',[$gallery['id']])}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>interný názov</label>
                                    <input type="text" name="internal_name" class="form-control" value="{{$gallery['internal_name']}}" required>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <h5>Existujúce obrázky v galérii</h5>
                                <div class="row">
                                    @if(isset($images) && !empty($images))
                                        @foreach($images as $image)
                                            <div class="col-md-3 img-remove cursor-pointer" data-id="{{$image['id']}}" style="height:200px;">
                                                <div style="width:100%;height:100%;background-image:url('{{$image['name']}}');background-position:center;background-size:cover;background-repeat: no-repeat;"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <div id="cloned-wrapper" class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12 clone-row">
                                        <div class="row">
                                            <div class="col-xs-10 col-sm-10 col-md-10">
                                                <div class="form-group">
                                                    <label>Obrázok</label>
                                                    <input type="file" class="form-control clone-input" name="image[0]">
                                                </div>
                                            </div>
                                            <div class="col-xs-2 col-sm-2 col-md-2">
                                                <button type="button" class="btn btn-success clone" style="margin-top:27px;">+</button>
                                                <button type="button" class="btn btn-danger remove" disabled style="margin-top:27px;">-</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-primary w-100">Uložiť</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>

        $('.img-remove').on('click',function(){
            if(confirm('Naozaj to chces zmazat?')){
                $(this).remove();
                $.ajax({
                    url:'{{route('gallery.destroy',['id'])}}',
                    type:'post',
                    data:{_method:'delete',_token:'{{csrf_token()}}',id:$(this).data().id},
                    dataType:'json',
                    success:function(data){

                    }
                });
            }
        });

        var counter = 1;
        $(document).on('click','.clone',function(){
            var cloned = $(this).closest('.clone-row').clone();
            cloned.find('.clone-input').val('');
            cloned.find('.clone-input').attr('name','image['+counter+']');
            cloned.find('.remove').attr('disabled',false);
            $('#cloned-wrapper').append(cloned);
            counter++;
            checkButtons();
        });
        $(document).on('click','.remove',function(){
            $(this).closest('.clone-row').remove();
            checkButtons();
        });

        var checkButtons = function(){
            var row = $('.clone-row');
            if(row.length>1){
                $('.remove').attr('disabled',false);
            }
            else{
                $('.remove').attr('disabled',true);
            }
        }
    </script>
@endsection
