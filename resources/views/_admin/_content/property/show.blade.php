@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Zariadenie'}}</h5>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <form action="{{route('property.update',[$property['id']])}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Zariadenie</h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>počet minút na odoslanie dalšieho emailu</label>
                                    <input type="number" step="1" min="0" max="59" name="minutes" class="form-control" value="{{!empty($property['minutes']) ? $property['minutes'] : 20}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>adresa</label>
                                    <input type="text" name="adress" class="form-control" value="{{$property['adress']}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>email</label>
                                    <input type="text" name="adress" class="form-control" value="{{$property['email']}}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>mesto</label>
                                    <input type="text" name="city" class="form-control" value="{{$property['city']}}" required>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <h5>Farby</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba navigácie</label>
                                            <input type="color" class="form-control" value="{{$property['navigation_color']}}" name="navigation_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba textu navigácie</label>
                                            <input type="color" class="form-control" value="{{$property['navigation_text_color']}}" name="navigation_text_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba hlavná ponuka</label>
                                            <input type="color" class="form-control" value="{{$property['content_color']}}" name="content_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba boxov</label>
                                            <input type="color" class="form-control" value="{{$property['box_color']}}" name="box_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba titulných nadpisov</label>
                                            <input type="color" class="form-control" value="{{$property['title_color']}}" name="title_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba textov</label>
                                            <input type="color" class="form-control" value="{{$property['text_color']}}" name="text_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Farba vstupov</label>
                                            <input type="color" class="form-control" value="{{$property['input_color']}}" name="input_color">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Primárna farba</label>
                                            <input type="color" class="form-control" value="{{$property['primary_color']}}" name="primary_color">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <h5>Text about</h5>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text fr</label>
                                    <textarea name="text[fr]" cols="30" rows="10" class="form-control summernote" required>{{isset($property['text']['fr']) ? $property['text']['fr'] : ''}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text en</label>
                                    <textarea name="text[en]" cols="30" rows="10" class="form-control summernote" required>{{isset($property['text']['en']) ? $property['text']['en'] : ''}}</textarea>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button class="btn btn-primary w-100">Uložiť</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                ]
            });
        });
    </script>
@endsection
