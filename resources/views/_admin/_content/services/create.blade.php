@extends('_admin._layouts.master')

@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>{{'Služby'}}</h5>
                        </div>
                    </div>
                    <legend></legend>
                </div>
                <div class="card-body">
                    <form action="{{route('services.store')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>interný názov</label>
                                    <input type="text" name="internal_name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Galéria</label>
                                    <select name="gallery_id" class="form-control">
                                        <option value="">Žiadna galéria</option>
                                        @if(isset($galleries) && !empty($galleries))
                                            @foreach($galleries as $gallery)
                                                <option value="{{$gallery['id']}}">{{$gallery['internal_name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Názov en</label>
                                    <input type="text" class="form-control" name="name[en]">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Názov fr</label>
                                    <input type="text" class="form-control" name="name[fr]">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Krátky popis en</label>
                                    <input type="text" class="form-control" name="short_text[en]">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Krátky popis fr</label>
                                    <input type="text" class="form-control" name="short_text[fr]">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Slug</label>
                                    <input type="text" class="form-control" name="slug">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text fr</label>
                                    <textarea name="text[fr]" cols="30" rows="10" class="form-control summernote" required>{{isset($property['text']['fr']) ? $property['text']['fr'] : ''}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Text en</label>
                                    <textarea name="text[en]" cols="30" rows="10" class="form-control summernote" required>{{isset($property['text']['en']) ? $property['text']['en'] : ''}}</textarea>
                                </div>
                            </div>
                            <legend></legend>
                            <div class="col-md-12">
                                <button class="btn btn-primary w-100">Uložiť</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                height: 120,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                ]
            });
        });
    </script>
@endsection
