<!-- Main navbar -->

<style>
    .activeLang{
        background-color:#10a69a;
        color:white;
    }
    .activeLang:hover{
        background-color:#10a69a;
        color:white;
    }
</style>
<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="{{url('/'.$currentLang.'/dashboard')}}" class="d-inline-block">
            <h5 style="margin:0;font-weight: bold;color:white;"><small>Dash</small>Board</h5>

        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <span>{{Auth::user()->email}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="{{route('logout')}}" class="dropdown-item"><i class="icon-switch2"></i>Odhlásiť</a>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->

<script>
    // $('#navbar-mobile li.dropdown').hover(function() {
    //     $(this).find('.dropdown-menu').first().show();
    // }, function() {
    //     var _this = $(this),
    //         _menus = _this.find('.dropdown-submenu').find('.dropdown-menu');
    //
    //     if(_menus.length>0){
    //         _menus.each(function(){
    //             if($(this).is(':visible')){
    //                 return false;
    //             }
    //             else{
    //                 _this.find('.dropdown-menu').first().hide();
    //             }
    //         });
    //     }
    //     else{
    //         _this.find('.dropdown-menu').first().hide();
    //     }
    // });

    $('#navbar-mobile .dropdown-submenu ').hover(function() {
        $(this).find('.dropdown-menu').first().show();
    }, function() {
        $(this).find('.dropdown-menu').first().hide();
    });
</script>
