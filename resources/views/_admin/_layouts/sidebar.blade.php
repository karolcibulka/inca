<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>

    <div class="sidebar-content">

        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs"> Web</div>
                    <i class="icon-menu" title="Web"></i>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang)}}" class="nav-link active">
                        <i class="icon-grid2"></i>
                        <span>Prejsť na web</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="sidebar-content">

        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs"> Administrácia</div>
                    <i class="icon-menu" title="Administrácia"></i>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/dashboard')}}" class="nav-link {{Request::segment(2) == 'dashboard' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/projects')}}" class="nav-link {{Request::segment(2) == 'projects' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Projekty</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/gallery')}}" class="nav-link {{Request::segment(2) == 'gallery' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Galérie</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/services')}}" class="nav-link {{Request::segment(2) == 'services' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Služby</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/reviews')}}" class="nav-link {{Request::segment(2) == 'reviews' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Hodnotenia</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/property')}}" class="nav-link {{Request::segment(2) == 'property' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Zariadenie</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/'.$currentLang.'/messages')}}" class="nav-link {{Request::segment(2) == 'messages' ? 'active' : ''}} ">
                        <i class="icon-grid2"></i>
                        <span>Správy</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
