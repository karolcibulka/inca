<section class="services mt-100 pb-100" data-scroll-index="1" id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-20 text-center">
                <h6 class="small-title">{{__('global.whatWeDo')}}</h6>
                <h4 class="title">{{__('global.ourServices')}}</h4>
            </div>
            @if(isset($services) && !empty($services))
                @foreach($services as $key => $service)
                    @php
                        $number = $key+1<10 ? '0'.($key+1) : $key;
                    @endphp
                    <div class="col-md-4">
                        <div class="item"
                            style="{{isset($service['images'][0]['medium']) && !empty($service['images'][0]['medium']) ? 'background-image:url('.$service['images'][0]['medium'].');' : ''}}">
                            <a href="{{url($currentLang.'/service/'.$service['slug'])}}" class="con">
                                <div class="numb">{{$number}}</div>
                                <h5>{{$service['name'][$currentLang]}}</h5>
                                <p>{{$service['short_text'][$currentLang]}}</p>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>
