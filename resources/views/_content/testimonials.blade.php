@if(isset($reviews) && !empty($reviews))
    <section class="testimonial bg-img bg-fixed pos-re mt-100 pt-100 pb-100" data-overlay-dark="6" data-background="{{asset('img/nice2.jpg')}}" style="background-position: center;">
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="testimonials">
                        <div class="owl-carousel owl-theme text-center">
                            @foreach($reviews as $review)
                                @if(isset($review['text'][$currentLang]) && !empty($review['text'][$currentLang]))
                                    <div class="item">
                                        <div class="client-area">
                                            <h6>{{$review['name']}}</h6> <span>{{$review['work']}}</span>
                                        </div>
                                        <p>" {{$review['text'][$currentLang]}} "</p>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
