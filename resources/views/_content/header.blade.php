<header class="header pos-re slider-fade" data-scroll-index="0">
    <div class="owl-carousel owl-theme">
        <div class="item bg-img" data-overlay-dark="3" data-background="{{asset('img/slider/2.jpg')}}">
            <div class="container text-center v-middle caption">
                <h1>INCA SARL, NICE</h1>
                <a href="#services" data-scroll-nav="1" class="btn"><span>{{__('global.discoverMore')}} <i class="ti-arrow-right"></i></span></a>
            </div>
        </div>
    </div>
</header>
