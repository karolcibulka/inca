<section class="contact mt-100 pb-100" data-scroll-index="5" id="contact">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12 mb-20">
                <h6 class="small-title">{{__('global.touch')}}</h6>
                <h4 class="title">{{__('global.contactUs')}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <!-- <div class="item bg-4">
                     <div class="con">
                         <h5>NICE</h5>
                         <p><i class="ti-home" style="font-size: 15px; color: #c5a47e;"></i> 15 Crow Pl, South Kensington, London, UK</p>
                         <p><i class="ti-mobile" style="font-size: 15px; color: #c5a47e;"></i> +44 20 1234 4444</p>
                         <p><i class="ti-envelope" style="font-size: 15px; color: #c5a47e;"></i> london@inca.com</p>
                     </div>
                 </div>-->
            </div>
            <div class="col-md-4">
                <div class="item bg-4">
                    <div class="con">
                        <h5>{{$property['city']}}</h5>
                        <p><i class="ti-home" style="font-size: 15px; color: {{$property['primary_color']}};"></i> {{$property['adress']}}</p>
                        <p><i class="ti-mobile" style="font-size: 15px; color: {{$property['primary_color']}};"></i> {{$property['telephone']}}</p>
                        <p><i class="ti-envelope" style="font-size: 15px; color: {{$property['primary_color']}};"></i> {{$property['email']}}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <!--<div class="item bg-4">
                    <div class="con">
                        <h5>NEW YORK</h5>
                        <p><i class="ti-home" style="font-size: 15px; color: #c5a47e;"></i> 363 W 16th St, New York, NY 10011, USA</p>
                        <p><i class="ti-mobile" style="font-size: 15px; color: #c5a47e;"></i> +1 212 12 4444</p>
                        <p><i class="ti-envelope" style="font-size: 15px; color: #c5a47e;"></i> newyork@inca.com</p>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="row mt-100">
            <div class="col-md-6 offset-md-3">
                <form class="form" id="contact-form" action="{{route('sendEmail')}}" method="post">
                    @csrf
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6" style="{{$sendEmail ? 'display:none' : ''}}">
                                <div class="form-group has-error has-danger">
                                    <input id="form_name" {{$sendEmail ? 'disabled' : ''}} type="text" name="name" placeholder="{{__('global.name')}}" required="required"> </div>
                            </div>
                            <div class="col-md-6" style="{{$sendEmail ? 'display:none' : ''}}">
                                <div class="form-group has-error has-danger">
                                    <input id="form_email" {{$sendEmail ? 'disabled' : ''}} type="email" name="email" placeholder="{{__('global.email')}}" required="required"> </div>
                            </div>
                            <div class="col-md-12" style="{{$sendEmail ? 'display:none' : ''}}">
                                <div class="form-group">
                                    <input id="form_subject" {{$sendEmail ? 'disabled' : ''}} type="text" name="subject" required="required" placeholder="{{__('global.subject')}}"> </div>
                            </div>
                            <div class="col-md-12" style="{{$sendEmail ? 'display:none' : ''}}">
                                <div class="form-group">
                                    <textarea id="form_message" {{$sendEmail ? 'disabled' : ''}} name="message" placeholder="{{__('global.message')}}" rows="4" required="required"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12" style="{{$sendEmail ? '' : 'display:none'}}">
                                <div class="alert alert-success border-radius0 text-white text-center current-background-color">
                                    <strong>{{__('global.message1')}} </strong><br>
                                    <small>{{__('global.message2')}}</small><br>
                                    {{__('global.message3')}} <strong>{{$sendEmail ? $countdownTime : ''}} {{$sendEmail && $countdownTime > 1 ?  __('global.minutes') : __('global.minute')}} </strong><br>


                                </div>
                            </div>
                            <div class="col-md-12 text-center" style="{{$sendEmail ? 'display:none' : ''}}">
                                <button type="submit"  {{$sendEmail ? 'disabled' : ''}} class="btn"><span>{{__('global.sendMessage')}} <i class="ti-arrow-right"></i></span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
