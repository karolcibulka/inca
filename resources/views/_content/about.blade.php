<section class="about mt-100 pb-100" data-scroll-index="2" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-20">
                <h6 class="small-title">{{__('global.aboutUs')}}</h6>
                <h4 class="title">{{__('global.about')}} INCA SARL</h4>
                {!! isset($property['text'][$currentLang]) && !empty($property['text'][$currentLang]) ? $property['text'][$currentLang] : '' !!}
            </div>
            <div class="col-md-6 mb-20 image">
                <div class="img"> <img src="{{asset('img/25.png')}}" alt="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="yearimg">
                    <div class="numb">{{(date('Y')-2010)}}</div>
                </div>
                <div class="year">
                    <h6 class="small-title">{{__('global.business')}}</h6>
                    <h4 class="title">{{__('global.experience')}}</h4>
                </div>
            </div>
        </div>
    </div>
</section>
