<style>
    .hidden {display: none;}
</style>
@if(isset($projects) && !empty($projects))
    <section class="projects mt-100 pb-100" data-scroll-index="3" id="projects">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-20 text-center">
                    <h6 class="small-title">{{__('global.portfolio')}}</h6>
                    <h4 class="title">{{__('global.ourProjects')}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme">
                        @foreach($projects as $key => $project)
                            <div class="item mb-50">
                                <div class="position-re o-hidden" style="background-image:url('{{$project['images'][0]['medium']}}');background-position:center;background-size:cover;height: 470px;">

                                </div>
                                <div class="con">
                                    <span class="category">
                                        <a href="{{url($currentLang.'/project/'.$project['slug'])}}">
                                            {{$project['category'][$currentLang]}}
                                        </a>
                                    </span>
                                    <h5>
                                        <a href="{{url($currentLang.'/project/'.$project['slug'])}}">
                                            {{$project['name'][$currentLang]}}
                                        </a>
                                    </h5>
                                    <a href="{{url($currentLang.'/project/'.$project['slug'])}}">
                                        <i class="ti-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
