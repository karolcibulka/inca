<style>
    .inca-fixed-sidebar .logo h1 {
        font-weight: 400;
        font-size: 75px;
        color: {{$property['primary_color']}} !important;
    }

    .inca-menu-copyright p a {
        color: {{$property['primary_color']}} !important;
    }

    .content-lines-inner {
        border-left: 1px solid {{$property['input_color']}} !important;
        border-right: 1px solid {{$property['input_color']}} !important;
        border-bottom: 1px solid {{$property['input_color']}} !important;
    }

    .content-lines {
        border-left: 1px solid {{$property['input_color']}} !important;
        border-right: 1px solid {{$property['input_color']}} !important;
    }

    .inca-menu-fixed > ul > li {
        border-bottom: 1px solid {{$property['input_color']}} !important;
    }

    footer {
        border-top: 1px solid {{$property['input_color']}} !important;
    }

    .btn {
        background-color: {{$property['primary_color']}};
        color:#fff !important;
    }

    .testimonials:before {
        border: 4px solid {{$property['content_color']}} !important;
    }

    .testimonials:after {
        border: 4px solid {{$property['content_color']}} !important;
    }

    span:hover, a:hover {
        color: {{$property['primary_color']}} !important;
    }

    .inca-menu-fixed > ul > li > a:hover{
       color:{{$property['primary_color']}} !important;
    }

    .inca-fixed-sidebar .inca-menu-social-media .social a::before {
        background-color: {{$property['primary_color']}} !important;
    }

    #loader {
        background-color: {{$property['content_color']}} !important;
    }

    #loader .loading {
        display: block;
        font-size: 0;
        color: {{$property['primary_color']}} !important;
    }

    .testimonials .client-area h6 {
        font-size: 30px;
        color: {{$property['content_color']}} !important;
    }

    .testimonials .client-area span {
        color: {{$property['input_color']}} !important;
    }

    .services .item:hover h5{
        color:{{$property['input_color']}} !important;
    }

    .inca-side-content > .logo {
        background-color: {{$property['primary_color']}};
        color:#ffffff !important;
    }

    .inca-side-content > .logo > h1 >a {
        color:#ffffff !important;
    }

    .inca-header-overlay .inca-burger-menu, .inca-fixed-sidebar + .inca-burger-menu {
        background-color: {{$property['primary_color']}};
    }

    .services .item:hover p{
        color:{{$property['content_color']}} !important;
    }

    .contact .item:hover h5{
        color:{{$property['input_color']}} !important;
    }

    .contact .item:hover p{
        color:{{$property['content_color']}} !important;
    }

    .contact .item:hover p i{
        color:{{$property['input_color']}} !important;
    }

    .testimonials p {
        color:{{$property['content_color']}} !important;
    }

    .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
        background: {{$property['primary_color']}} !important;
    }

    .current-background-color{
        background-color: {{$property['primary_color']}} !important;
        border-color: {{$property['primary_color']}} !important;
    }

    .services .item h5 {
        color: {{$property['primary_color']}} !important;
    }

    .small-title {
        color: {{$property['primary_color']}} !important;
    }

    h1, h2, h3, h4, h5, h6 {
        color: {{$property['primary_color']}};
    }

    .pointer{
        cursor:pointer;
    }

    .about .numb {
        color: {{$property['primary_color']}} !important;
    }

    .projects .item .con .category {
        color: {{$property['primary_color']}} !important;
    }

    .testimonials {
        border: 8px solid {{$property['content_color']}} !important;
    }

    .contact .item h5 {
        color: {{$property['primary_color']}} !important;
    }

    /*#c5a47e*/



    .inca-fixed-sidebar .inca-menu-social-media .social a i:hover {
        color:white;
    }

    .inca-fixed-sidebar {
        background-color: {{$property['navigation_color']}} !important;
    }

    .inca-menu-fixed > ul > li > a {
        color: {{$property['navigation_text_color']}} !important;
    }

    .contact .item:after {
        background: {{$property['box_color']}} !important;
    }

    .services .item:after {
        background: {{$property['box_color']}} !important;
    }

    .inca-fixed-sidebar .inca-menu-social-media .social a i {
        color: {{$property['navigation_text_color']}} !important;
    }

    .inca-fixed-sidebar .logo a span {
        color: {{$property['navigation_text_color']}} !important;
    }

    .inca-menu-copyright p {
        color: {{$property['navigation_text_color']}} !important;
    }

    .contact .form input, .contact .form textarea {
        background-color: {{$property['input_color']}} !important;
    }

    .title {
        color: {{$property['title_color']}} !important;
    }

    body{
        background-color:{{$property['content_color']}} !important;
        color: {{$property['text_color']}} !important
    }

    p {
        color: {{$property['text_color']}} !important
    }

    footer p {
        color: {{$property['text_color']}} !important
    }

    .header .caption h4{
        color: {{$property['title_color']}} !important;
    }
    .header .caption h1 {
        font-size: 75px;
        color: {{$property['title_color']}};
    }

    .btn:hover{
        color: #fff;
    }

    [data-overlay-dark] h1, [data-overlay-dark] h2, [data-overlay-dark] h3, [data-overlay-dark] h4, [data-overlay-dark] h5, [data-overlay-dark] h6, [data-overlay-dark] span {
        color: #fff;
    }

    .btn span:hover {
        color: #ffffff !important;
    }

    .btn:hover span {
        color: #ffffff !important;
    }

    .post .item .spical {
        margin: 15px;
        padding: 15px;
        border-left: 4px solid {{$property['primary_color']}};
        font-style: italic;
    }


    /*.btn:hover{*/
    /*    background-color: darkgrey !important;*/
    /*}*/

</style>
