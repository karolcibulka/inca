<div class="inca-fixed-sidebar inca-sidebar-left">
    <div class="inca-header-container">
        <!-- Logo -->
        <div class="logo">
            @if(Request::segment(2) == 'project' || Request::segment(2) == 'service')
                <h1><a href="{{url($currentLang.'/')}}">INCA SARL<span>{{__('global.reconstructions')}}</span></a></h1>
            @else
                <h1><a href="#">INCA SARL<span>{{__('global.reconstructions')}}</span></a></h1>
            @endif
        </div>
        <!-- Burger menu -->
        <div class="inca-burger-menu">
            <div class="inca-line-menu inca-line-half inca-first-line"></div>
            <div class="inca-line-menu"></div>
            <div class="inca-line-menu inca-line-half inca-last-line"></div>
        </div>
        <!-- Navigation menu -->
        <nav class="inca-menu-fixed">
            @if(Request::segment(2) == 'project' || Request::segment(2) == 'service')
                <ul>
                    <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#home">{{__('global.home')}}</a></li>
                    @if(isset($services) && !empty($services))
                        <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#services">{{__('global.services')}}</a></li>
                    @endif
                    <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#about">{{__('global.aboutUs')}}</a></li>
                    @if(isset($projects) && !empty($projects))
                        <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#projects">{{__('global.projects')}}</a></li>
                    @endif
                    @if(isset($blogs) && !empty($blogs)):?>
                    <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#blog">{{__('global.news')}}</a></li>
                    @endif
                    <li><a href="{{Request::segment(2) == 'project' || Request::segment(2) == 'service' ? url($currentLang.'/') : ''}}#contact">{{__('global.contact')}}</a></li>
                </ul>
            @else
                <ul>
                    <li><a href="#home" data-scroll-nav="0">{{__('global.home')}}</a></li>
                    @if(isset($services) && !empty($services))
                        <li><a href="#services" data-scroll-nav="1">{{__('global.services')}}</a></li>
                    @endif
                    <li><a href="#about" data-scroll-nav="2">{{__('global.aboutUs')}}</a></li>
                    @if(isset($projects) && !empty($projects))
                        <li><a href="#projects" data-scroll-nav="3">{{__('global.projects')}}</a></li>
                    @endif
                    @if(isset($blogs) && !empty($blogs)):?>
                    <li><a href="#blog" data-scroll-nav="4">{{__('global.news')}}</a></li>
                    @endif
                    <li><a href="#contact" data-scroll-nav="5">{{__('global.contact')}}</a></li>
                </ul>
            @endif

        </nav>
        <!-- Menu social media -->
        <div class="inca-menu-social-media">
            <div class="social">
                <a href="https://www.facebook.com/ionut.nicecotedazure"><i class="ti-facebook"></i></a>
                <!--<a href="#"><i class="ti-twitter"></i></a>
                <a href="#"><i class="ti-instagram"></i></a>-->
            </div>
            <div class="languages">
                <a href="{{url('/fr')}}"><img class="flag {{$currentLang != 'fr' ?'': 'active'}}" src="{{asset('img/flags/fr.svg')}}" alt=""></a>
                <a href="{{url('/en')}}"><img class="flag {{$currentLang != 'en' ?'': 'active'}}" src="{{asset('img/flags/en.svg')}}" alt=""></a>
            </div>
            <div class="inca-menu-copyright">
                <p>© {{date('Y')}} INCA SARL {{__('global.by')}} <a href="https://each.sk/">each.sk</a></p>
            </div>
        </div>
    </div>
</div>

