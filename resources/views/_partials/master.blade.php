<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!--start meta data-->
    <title>INCA SARL {{isset($row['name'][$currentLang]) && !empty($row['name'][$currentLang]) ? '- '.$row['name'][$currentLang] : ''}}</title>
    <meta name="description" content="{{isset($row['text'][$currentLang]) && !empty($row['text'][$currentLang]) ? Str::limit(strip_tags($row['text'][$currentLang]),156) : Str::limit(strip_tags($property['text'][$currentLang]))}}">
    <meta name="robots" content="index, follow">
    <!-- start og -->
    <meta property="og:type" content="article" />
    <meta property="og:title" content="INCA SARL {{isset($row['name'][$currentLang]) && !empty($row['name'][$currentLang]) ? '- '.$row['name'][$currentLang] : ''}}" />
    <meta property="og:description" content="{{isset($row['text'][$currentLang]) && !empty($row['text'][$currentLang]) ? Str::limit(strip_tags($row['text'][$currentLang]),156) : Str::limit(strip_tags($property['text'][$currentLang]))}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:site_name" content="INCA SARL"/>
    <!-- end og-->

    <!-- end meta data-->


    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('css/menu.css')}}" />
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/YouTubePopUp.css')}}" />
    <link rel="stylesheet" href="{{asset('css/owl-carousel.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/owl.css')}}" />
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <link rel="stylesheet" href="{{asset('css/fancy.css')}}" />
</head>

<body>
@include('_partials.custom_style')
<!-- Loader -->
<div id="loader">
    <div class="loading">
        <div></div>
    </div>
</div>

<!-- Sidebar -->
@include('_partials.navigation')

<!-- Content -->
<div class="inca-side-content">
    <!-- Lines -->
    <div class="content-lines-wrapper">
        <div class="content-lines-inner">
            <div class="content-lines"></div>
        </div>
    </div>
    @yield('content')
    <!-- Footer -->
    @include('_partials.footer')
</div>

<!-- jQuery -->
<script src="{{asset('js/jquery.js')}}"></script>

<script src="{{asset('js/fancy.js')}}"></script>
<script src="{{asset('js/popper.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/scrollIt-min.js')}}"></script>
<script src="{{asset('js/wapoints.js')}}"></script>
<script src="{{asset('js/owl.js')}}"></script>
<script src="{{asset('js/stellar.js')}}"></script>
<script src="{{asset('js/YouTubePopUp.js')}}"></script>
<script src="{{asset('js/validator.js')}}"></script>
<script src="{{asset('js/autotype.js')}}"></script>
<script src="{{asset('js/menu.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
</body>

</html>
