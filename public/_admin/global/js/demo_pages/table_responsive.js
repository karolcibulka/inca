/* ------------------------------------------------------------------------------
 *
 *  # Responsive tables
 *
 *  Demo JS code for table_responsive.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup modules
// ------------------------------

var TableFootable = function() {


    //
    // Setup modules components
    //

    // Default file input style
    var _componentFootable = function() {
        if (!$().footable) {
            console.warn('Warning - footable.min.js is not loaded.');
            return;
        }

		// Initialize responsive functionality
	    $('.table-togglable').footable();
    };


    //
    // Return objects assigned to modules
    //

    return {
        init: function() {
            _componentFootable();
        }
    }
}();


// Initialize modules
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    TableFootable.init();
});
